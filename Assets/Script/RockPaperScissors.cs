﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RockPaperScissors : MonoBehaviour {    

    [SerializeField]
    private Dropdown modeDrpDwn = null;
    [SerializeField]
    private Text playerTxt = null;
    [SerializeField]
    private Dropdown weaponDrpDwn = null;
    [SerializeField]
    private Button playBtn = null;
    [SerializeField]
    private Text adversaryTxt = null;
    [SerializeField]
    private Text weaponAdversaryTxt = null;
    [SerializeField]
    private Text resultTxt = null;
    [SerializeField]
    private Text janKenPoTxt = null;
    [SerializeField]
    private Text winsTxt = null;
    [SerializeField]
    private Text losesTxt = null;
    [SerializeField]
    private Text tiesTxt = null;

    private GameMode gameMode = null;

    private List<string> weaponList = null;

    private int wins = 0;
    private int loses = 0;
    private int ties = 0;

    private bool[,] beatArray = null;

    // Use this for initialization
    void Start () {

        weaponList = new List<string> { "Scissors", "Paper", "Rock"};

        // if it is posible to extend, add in the list Lizard and Spock
        //weaponList.AddRange(new List<string> { "Lizard", "Spock" });

        // weaponList.Count must be odd number to be balanced
        if (weaponList.Count % 2 == 0)
            Debug.Log("Unbalanced game");

        weaponDrpDwn.AddOptions(weaponList);

        weaponAdversaryTxt.text = "";

        beatArray = new bool[weaponList.Count, weaponList.Count];

        int maxBeat = (weaponList.Count - 1) / 2;

        for (int i = 0; i < weaponList.Count; ++i)
        {
            for(int j = 1; j <= maxBeat; ++j)
            {
                int beat = (i + 2 * j - 1) % weaponList.Count;
                beatArray[i, beat] = true;
            }
        }

        SelectMode();
    }

    public void SelectMode()
    {
        if (modeDrpDwn.value == 0)
            gameMode = new PlayerVsCpu();
        else
            gameMode = new CpuVsCpu();

        gameMode.WeaponDrpDwn = weaponDrpDwn;
        gameMode.MaxWeapons = weaponList.Count;

        tiesTxt.text = "Ties: 0";
        winsTxt.text = gameMode.GetWinPlayer() + 0;
        losesTxt.text = gameMode.GetLosesPlayer() + 0;

        ties = wins = loses = 0;

        resultTxt.gameObject.SetActive(false);

        playerTxt.text = gameMode.GetPlayerName();
        adversaryTxt.text = gameMode.GetAdversaryName();
        weaponDrpDwn.interactable = gameMode.WeaponDrpDwnInteractable();
    }

    public void Play()
    {
        StartCoroutine(JanKenPoAnim());
    }

    IEnumerator JanKenPoAnim() {

        string[] janKenPo = new string[3]{ "Rock", "Paper", "Scissors"};

        ShowResultTxt(false);

        for (int i = 0; i < janKenPo.Length; ++i)
        {
            janKenPoTxt.text = janKenPo[i];

            yield return new WaitForSeconds(0.5f);
        }

        ShowResult();
    }

    private void ShowResultTxt(bool show)
    {
        adversaryTxt.gameObject.SetActive(show);
        weaponAdversaryTxt.gameObject.SetActive(show);
        resultTxt.gameObject.SetActive(show);
        playBtn.interactable = show;
        janKenPoTxt.gameObject.SetActive(!show);

        weaponDrpDwn.interactable = (show && gameMode.WeaponDrpDwnInteractable());
    }

    private void ShowResult() {
        int indexWeaponPlayer = gameMode.GetPlayerIndex();
        int indexWeaponAdversary = gameMode.GetRandomWeaponIndex();

        weaponAdversaryTxt.text = weaponList[indexWeaponAdversary];

        ShowResultTxt(true);

        // tie
        if (indexWeaponPlayer == indexWeaponAdversary)
        {
            resultTxt.text = "Tie";
            tiesTxt.text = "Ties: " + ++ties;

            return;
        }

        // win
        if (this.beatArray[indexWeaponPlayer, indexWeaponAdversary]) {
            resultTxt.text = gameMode.GetResultPlayerWin();
            winsTxt.text = gameMode.GetWinPlayer() + ++wins;

            return;
        }

        // lose
        resultTxt.text = gameMode.GetResultPlayerLose();
        losesTxt.text = gameMode.GetLosesPlayer()+ ++loses;
    }
}