﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMode {

    protected int maxWeapons;
    protected Dropdown weaponDrpDwn = null;

    public int MaxWeapons
    {
        set
        {
            maxWeapons = value;
        }
    }    

    public Dropdown WeaponDrpDwn
    {
        set
        {
            weaponDrpDwn = value;
        }
    }

    public virtual string GetWinPlayer() { return ""; }

    public virtual string GetResultPlayerWin() { return ""; }

    public virtual string GetLosesPlayer() { return ""; }

    public virtual string GetResultPlayerLose() { return ""; }

    public virtual int GetPlayerIndex() { return 0; }

    public int GetRandomWeaponIndex()
    {
        return Random.Range(0, maxWeapons);
    }

    public virtual string GetPlayerName() { return ""; }

    public virtual string GetAdversaryName() { return ""; }

    public virtual bool WeaponDrpDwnInteractable()
    {
        return true;
    }
}
