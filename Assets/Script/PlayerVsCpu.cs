﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVsCpu : GameMode {
    

    public override string GetWinPlayer()
    {
        return "Wins: ";
    }

    public override string GetResultPlayerWin()
    {
        return "You WIN!";
    }

    public override string GetLosesPlayer()
    {

        return "Loses: ";
    }

    public override string GetResultPlayerLose()
    {
        return "You LOSE";
    }

    public override int GetPlayerIndex()
    {
        return weaponDrpDwn.value;
    }

    public override string GetPlayerName()
    {
        return "Weapon";
    }

    public override string GetAdversaryName()
    {
        return "Adversary";
    }    
}