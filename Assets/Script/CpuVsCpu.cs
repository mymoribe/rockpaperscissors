﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CpuVsCpu : GameMode
{

    public override string GetWinPlayer()
    {
        return "Player 1 wins: ";
    }

    public override string GetResultPlayerWin()
    {
        return "Player 1 WIN!";
    }

    public override string GetLosesPlayer()
    {
        return "Player 2 wins: ";
    }

    public override string GetResultPlayerLose()
    {
        return "Player 2 WIN!";
    }

    public override int GetPlayerIndex()
    {
        int index = this.GetRandomWeaponIndex();

        weaponDrpDwn.value = index;

        return index;
    }

    public override string GetPlayerName()
    {
        return "Player 1";
    }

    public override string GetAdversaryName()
    {
        return "Player 2";
    }

    public override bool WeaponDrpDwnInteractable()
    {
        return false;
    }
}